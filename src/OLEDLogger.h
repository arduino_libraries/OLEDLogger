/*
OLEDLogger.h - Library for super simple access to 128x64 OLED.
Created by Tony Alpskog, 2016.
Released into the public domain.
*/

#ifndef OLEDLogger_h
#define OLEDLogger_h

#if defined (ARDUINO) && ARDUINO >= 100
  #include <Arduino.h>
#else
  #include <WProgram.h>
  #include <pins_arduino.h>
#endif

#if defined (__AVR__)
  #include <avr/io.h>
  #include <avr/interrupt.h>
#endif

#include <OLED_I2C.h>

extern uint8_t SmallFont[];//Max 21 visible chars horizontally. Ovreflow will be written outside the left edge of the screen.

class OLEDLogger : public OLED
{
public:
  OLEDLogger();
  void begin();
  void print(String);
  void clear();
private:
  String _stringArray[7] = { "", "", "", "", "", "", "" };
  const int IndexLinkArray[13] = { 1, 2, 3, 4, 5, 6, 0, 1, 2, 3, 4, 5, 6 };
  int _indexOfLastRow;
};

#endif


