/*
OLEDLogger.cpp - Library for super simple access to 128x64 OLED.
Created by Tony Alpskog, 2016.
Released into the public domain.
*/

#include "OLEDLogger.h"

OLEDLogger::OLEDLogger() : OLED(PIN_WIRE_SDA, PIN_WIRE_SCL, 8)
{
}

void OLEDLogger::begin()
{
  OLED::begin();
  setFont(SmallFont);
  setBrightness(1);
  _indexOfLastRow = 0;
}

void OLEDLogger::print(String text)
{
  clrScr();
  _indexOfLastRow = IndexLinkArray[_indexOfLastRow];
  _stringArray[_indexOfLastRow] = text;
  for (int arrayIndex = 0; arrayIndex < 7; arrayIndex++) {
    OLED::print(_stringArray[IndexLinkArray[arrayIndex + _indexOfLastRow]], LEFT, arrayIndex * 9);
  }
  update();
}

void OLEDLogger::clear()
{
  clrScr();
  update();
}


