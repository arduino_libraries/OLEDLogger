# OLEDLogger 
----
OLEDLogger makes it super easy to write output strings to a 128x64 I2C/TWI OLED display.
This library was made as a log tool for those projects that had to be disconnected from a computer.
Since serial logging not is available in those cases, a small OLED display is used instead.


![Running RowCountPrint example sketch](https://gitlab.com/arduino_libraries/OLEDLogger/raw/5d7fce5518aadda87e5105e7edfd1d608c1b0788/img/RowCountPrint.jpg)
Running RowCountPrint example sketch

## Releases
---
#### v1.0.0 30-Dec-2016
* No initialization parameters allowed. (Default OLED settings)
* Only one size text on OLED.
* 7 rows of left aligned text starting from down-up

## License information
-------------------
"OLEDLogger" is a derivative of "[*OLED_I2C*](http://www.rinkydinkelectronics.com/library.php?id=79)" by Henning Karlsen @ Rinky-Dink Electronics, used under [*CC BY-NC-SA 3.0*](https://creativecommons.org/licenses/by/3.0/).
"OLEDLogger" is [*licensed*](https://gitlab.com/arduino_libraries/OLEDLogger/tree/master/license) under [*CC BY-NC-SA 3.0*](https://creativecommons.org/licenses/by/3.0/) by Tony Alpskog.