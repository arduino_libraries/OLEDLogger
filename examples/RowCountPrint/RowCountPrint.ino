#include "OLEDLogger.h"

const String counterText = "Input text ";
int loopCounter = 1;
OLEDLogger oledDisplay;

void setup() {
  oledDisplay.begin();
  oledDisplay.print("OLED setup done");
}

void loop() {
  delay(2000);
  oledDisplay.print(counterText + loopCounter);
  loopCounter++;
}


